clear all

load figS1B.data;

A=figS1B;

stratio=A(:,1);

s1=A(:,2);
s2=A(:,3);

for i=1:size(stratio)
s1(i)=s1(i)/10;
s2(i)=s2(i)/10;
end


plot(stratio,s1,'r')
hold on
plot(stratio,s2,'g')


xlim([1 2])
ylim([110 135])

xlabel('stiffness ratio');
ylabel('principal stress (KPa)')


legend('stress 1','stress 2')