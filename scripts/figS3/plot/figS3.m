clear all

load figS3.data;

D=figS3;

teta=D(:,1);
steta=D(:,2);
pteta=D(:,3);

for i=1:size(teta)
teta(i)=(teta(i)*180)/3.14;
steta(i)=(steta(i)*180)/3.14;
pteta(i)=(pteta(i)*180)/3.14;
if (teta(i)<0 || teta(i)>=90) % for angle correction as the functions cos and acos were used
    pteta(i)=-pteta(i);
else
    steta(i)=-steta(i);
end
if teta(i)>=90
    pteta(i)=pteta(i)+180;
end
end


plotyy(teta,steta,teta,pteta)

[AX,H1,H2] = plotyy(teta,steta,teta,pteta,'plot');
set(AX(1),'ylim',[-2.5 3.5],'ytick',[-3 -1.5 0 1.5  3],'fontsize',30);
set(AX(2),'ylim',[-100 140],'ytick',[-90 -60 -30 0 30 60 90 120],'fontsize',30);
set(get(AX(1),'Ylabel'),'String','principal stress direction (deg)','fontsize',30) 
set(get(AX(2),'Ylabel'),'String','perpendicular to strain (deg)','fontsize',30) 



set(AX(1),'Xlim',[-45,135],'XTick',[-30 0 30 60 90 120],'fontsize',30); %,'XTicklabel',xticklabels)
set(AX(2),'Xlim',[-45,135],'XTick',[]); %,'XTicklabel',xticklabels)

grid on
xlabel('anisotropy direction (deg)','fontsize',30);


