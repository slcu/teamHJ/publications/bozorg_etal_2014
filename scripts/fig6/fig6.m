clear all

k=0.4
n=2
ym=50;
yf=150

a=0:.01:1;
for i=1:101
yl(i)=ym+.5*(  1+        (   a(i)^n        )        /( ((1-a(i))^n)*k^n+ a(i)^n  )         )*yf;
yt(i)=ym+.5*(  1-        (   a(i)^n        )        /( ((1-a(i))^n)*k^n+ a(i)^n  )         )*yf;
end

plot(a,yl,'r');
hold on
plot(a,yt,'g');
ylim([ym-20,ym+yf+20])
xlabel('anisotropy measure');
ylabel('Young modulus');
legend('longitudinal','transverse')
set(gca,'Xlim',[0,1],'XTick',[0 0.5 1],'fontsize',30);
set(gca,'YTick',[50 125 200],'fontsize',30);



