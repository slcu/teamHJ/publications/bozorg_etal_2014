clear all
format long
load parascanS2A.data;

g=parascanS2A;


for i=1:61
  for j=1:51
    
 teta(i,j)=(g(51*(i-1)+j,5));
end  
 
end

colormap(gray)


 imagesc(teta')



set(gca,'xTick',[1 16 31 46 61],'fontsize',30)
set(gca,'xTickLabel',{'1','2', '3', '4', '5'})
set(gca,'yTick',[1 11 21 31 41 51],'fontsize',30)
set(gca,'yTickLabel',{'1','0.8', '0.6', '0.4', '0.2', '0'})


ylabel('force anisotropy');
xlabel('elasticity ratio')

hcb=colorbar;
set(hcb,'YTick',[0,1],'fontsize',30);
ylabel(hcb,'cos(stress,strain)','fontsize',30)