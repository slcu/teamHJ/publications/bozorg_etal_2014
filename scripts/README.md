## GENERATING THE RESULT AND FIGURES

In this document it will be assumed that there is a global access to
the executables 'auto' and 'tSim' (which we have as link to the tissue
simulator), and that all 'Cells-lite' (FEM) simulations are performed
in the Cells-lite main directory (see Readme file in 'sourceCodes'
directory).

In addition, MATLAB (http://www.mathworks.com) scripts are used to
create plots and PARAVIEW (http://www.paraview.org) is used to
visualize results on different geometries.

### FIGURE 2A-2B:

TISSUE: In the folder 'scripts/fig2/AB run: 

> auto 2ABP 2.auto 

remove the directory 'autodata' and run: 

> auto 2ABP 4.auto 

This should create the files 'scripts/fig2/AB/plot/fig2aP2.data' and 'scripts/fig2/AB/plot/fig2aP4.data'.

NOTE: here and in many places the output is appended to files, i.e. you will need 
to remove the output files if a second run is executed.

FEM: Build and run 'figure2ab' executable in the folder 'sourceCodes/Cells-lite'. 
It will produce the files fig2aP2shell.data and fig2aP4shell.data in the sub-folder 
'sourceCodes/Cells-lite/output'. Copy both files to the folder 'scripts/fig2/AB/plot'.

PLOT: Run the MATLAB scripts 'fig2A.m' and 'fig2B.m' from the 'scripts/fig2/AB/plot' directory. 

### FIGURE 2C 

TISSUE: In the folder 'scripts/fig2/C' run: 

> auto 2C.auto

The file 'scripts/fig2/C/plot/fig2C.data' should be created.

PLOT: In the folder 'scripts/fig2/C/plot' run the MATLAB script 'fig2C.m'. 
Young modulus and Poisson ratio should be scaled. 

### FIGURE 2D 

TISSUE: In the folder 'scripts/fig2/D' run: 

> auto 2D.auto 

The file 'scripts/fig2/D/plot/fig2D.data' should be created.

FEM: Build and run 'figure2d' executable in the folder 'sourceCodes/Cells-lite'. 
It will produce the file 'fig2Dshell.data' in the sub-folder 'sourceCodes/Cells-lite/output'.
Copy the file to the folder 'scripts/fig2/D/plot'.

PLOT: In the folder 'scripts/fig2/D/plot' run the MATLAB script 'fig2D.m'. 

### FIGURE 2E 

TISSUE: In the folder 'scripts/fig2/EF' run: 

> tSim 2EF.model 2EF.init 2EF.solver > plot/strainTRBS.dat 

The results will be stored in the sub-folder 'scripts/fig2/EF/vtk' as a time series 
in vtk format and can be visualized by Paraview. Move the output files from 
'scripts/fig2/EF/vtk' to the folder 'scripts/fig2/EF/TRBS'.
 
FEM: Build and run 'figure2ef' executable in the folder 'sourceCodes/Cells-lite'. 
In the sub-folder 'sourceCodes/Cells-lite/output' it will produce the file 'missesStrainShell.dat' 
and the file 'result.pvd' which can be visualized with Paraview. 
Copy the file 'result.pvd' to the folder 'scripts/fig2/EF/shells'. 

PLOT: Load Paraview state-file 'scripts/fig2/EF/2E.pvsm' in Paraview.  This will 
visualize the result using the results from the folders 'scripts/fig2/EF/TRBS' and 
'/scripts/fig2/EF/shells' (make sure the paths to the data files are correct when 
loading the pvsm file). 

### FIGURE 2F 

PLOT: Move the file 'sourceCodes/Cells-lite/output/missesStrainShell.dat' generated 
by FEM for FIG 2E to the directory 'scripts/fig2/EF/plot'. 
In the folder 'scripts/fig2/EF/plot' run the MATLAB script 'fig2F.m'.
 
### FIGURE 3A 

TISSUE: In the folder 'scripts/fig3/ADG/A' run: 

> auto 3A.auto 

This should generate the file 'scripts/fig3/ADG/A/plot/parascan3A.data'.

PLOT: In the folder 'scripts/fig3/ADG/A/plot' run the MATLAB script 'fig3A.m'. 
 
### FIGURE 3B and 3C 

TISSUE: In the folder 'scripts/fig3/BC' run: 

> tSim 3BC.model capcylinderscaled.init solver.rk5 > plot/fig3C.dat 

The results will be stored in the sub-folder 'scripts/fig3/BC/vtk' as a time series 
in vtk format and could be visualized by using Paraview. 

PLOT: To plot different quantities using Paraview, select correct cell variables 
(see the bottom of the file 'scripts/fig3/BC/3BC.model for which cell variables that 
correspond to which quantity) (Fig3B). For plotting the angular distribution, run 
the MATLAB script 'plot3C.m' in the directory 'scripts/fig3/BC/plot'. (Fig3C).
 
### FIGURE 3D,3E,3F and 3G,3H,3K

Follow the same procedure as for 3A-C to generate the result for 3D-F and 3G-H but in the 
corresponding folders.

### FIGURE 4A, 4B and 4C 

TISSUE: In the folder 'scripts/fig4/ABC' run: 

> tSim 4ABC.model parabolidscaled.init solver.rk5 > volume.dat 

PLOT: The results will be stored in the folder 'scripts/fig4/ABC/vtk' as a time 
series in vtk format and could be visualized by using Paraview. To plot different 
quantities using Paraview, select correct cell variables (see the bottom of the file 
'scripts/fig4/ABC/4ABC.model for which cell variables that correspond to which quantity). 

### FIGURE 4D, 4E and 4F 

TISSUE: In the folder 'scripts/fig4/DEF' run: 

> tSim 4DEF.model doublescaled.init solver.rk5 > volume.dat 

PLOT: The results will be stored in the sub-folder 'scripts/fig4/DEF/vtk' as a time series 
in vtk format and could be visualized by using Paraview. To plot different quantities 
using Paraview, select correct cell variables (see the bottom of the file 
'scripts/fig4/DEF/4DEF.model for which cell variables that correspond to which quantity).
 
### FIGURE 5A, 5B and 5C 

TISSUE: In the folder 'scripts/fig5/ABC/1/iso' run: 

> tSim template.model ellipsoid1.init solver.rk5 

Do the same in the folders 'scripts/fig5/ABC/1/MSD' and 'scripts/fig5/ABC/1/OsD'. 

Do the same in the folders where '1' is replaced by 2, 3, 4 and 5.

PLOT: In all 15 folders, the results will be stored in a sub-folder
'vtk' as a time series in vtk format and could be visualized by
Paraview. To create Fig. 5B,5C we have extracted the
dimensions (manually) in Paraview. In the figure 5B dimensions of the
deformed templates under different conditions were measured manually
in Paraview. Also for calculating normalized average strain anisotropy shown in 
Figure 5C strain anisotropy of individual elements which were stored in the 17th 
component of the cell vectors were averaged by Paraview.

### FIGURE 5D

TISSUE: In the folder 'scripts/fig4/DEF/MTp' run:

> tSim 4MTp.model doublescaled.init solver.rk5 > volume.dat

PLOT: The results will be stored in the sub-folder 'scripts/fig4/DEF/vtk' and 
'scripts/fig4/DEF/MTp/vtk' as time series in vtk format and could be visualized 
using Paraview.

### FIGURE S1A 

FEM: Build and run 'figureS1a' executable in the folder 'sourceCodes/Cells-lite'. 
It will produce the file 'fig2CShell.data' in the sub-folder 'sourceCodes/Cells-lite/output'.

PLOT: Copy the files ' scripts/fig2/C/plot/fig2C.data' and 'sourceCodes/Cells-lite/output/fig2CShell.data' 
to the folder 'scripts/figS1/A/plot'. In the folder 'scripts/figS1/A/plot' run the MATLAB script 'figS1A.m'.

### FIGURE S1B 

TISSUE: In the folder 'scripts/figS1/B' run:

> auto S 1B.auto 

This should generate the output file 'scripts/figS1/B/plot/figS1B.data'.

PLOT: In the folder 'scripts/figS1/B/plot' run the MATLAB script 'figS1B.m'. 

### FIGURE S1C 

TISSUE: In the folder 'scripts/figS1/C' run: 

> tSim S1C.model S1C.init solver.rk5 > volume.dat 

PLOT: The results will be stored in the sub-folder 'scripts/figS1/C/vtk' as a time 
series in vtk format and could be visualized by using Paraview. To plot different 
quantities using Paraview, select correct cell variables (see the bottom of the file 
'scripts/figS1/C/S1C.model for which cell variables that correspond to which quantity).

### FIGURE S1D, S1E and S1F 

TISSUE: The steps are similar to S1C in the corresponding folders. 

FEM: Build and run 'figureS1c', 'figureS1d', figureS1e', 'figureS1f'
executables in the folder 'sourceCodes/Cells-lite'. Each of them will
produce a pvd file in the sub-folder 'sourceCodes/Cells-lite/output'
named 'figureS1[c,d,e,f].pvd'. These can be visualized with Paraview. 

### FIGURE S2 

TISSUE: In the folder 'scripts/figS2/A' run: 

> auto S2A.auto

This should generate the output file 'scripts/figS2/A/plot/parascanS2A.data'.

PLOT: In the folder 'scripts/figS2/A/plot' run the MATLAB script 'figS2A.m'. 

Do the same for S2B and S2C in the corresponding folders. 

### FIGURE S3 

TISSUE: In the folder 'scripts/figS3' run: 

> auto S3.auto 

This should generate the output file 'scripts/figS3/plot/figS3.data'.

PLOT: In the folder 'scripts/figS3/plot' run the MATLAB script 'figS3.m'.

### FIGURE S4
 
TISSUE: In the folder 'scripts/figS4/A' run: 

> tSim S4A.model S4.init solver.rk5 

PLOT: The results will be stored in the folder 'scripts/figS4/A/vtk' as a time 
series in vtk format and could be visualized by using Paraview. Load paraview-state-file 
'scripts/figS4/S4.pvsm' in Paraview to plot the result (make sure the right vtu 
output files are used when loading the pvsm file).  

Do the same for S4B and S4C in the corresponding folders. 

### FIGURE S5

PLOT: PLotted using Paraview and the the output data generated for Fig4A-C (other 
cell variables selected for S5A,B), FigD-F (other cell variables selected for S5C-D) 
and Fig5D (other cell variables plotted in S5E).

### FIGURE S6A and S6B 

TISSUE: In the folder 'scripts/figS6/A' run: 

> tSim S6A.model S6A.init solver.rk5 

In the folder 'scripts/figS6/B' run: 

> tSim S6B.model S6B.init solver.rk5 

PLOT: The results will be stored in the folders 'scripts/figS6/A/vtk' and 
'scripts/figS6/B/vtk' as time series in vtk format and could be visualized by 
using Paraview. To plot different quantities using Paraview, select correct cell 
variables (see the bottom of the files 'scripts/figS6/A/S6A.model' and 
'scripts/figS6/B/S6B.model' for which cell variables that correspond to which 
quantity). In Paraview load 'hypocotyl.pvsm' in the folder 'scripts/figS6' 
(and make sure the vtu files to be loaded are correct). 

### MOVIE S1 

Time series movies saved from Paraview, same data as used for Fig3E and Fig3H. 

### MOVIE S2 

Time series movies saved from Paraview, same data as used for Fig4D-F and Fig5D. 
