
clear all
format long
load fig2C.data;

g=fig2C;
scale=10;

for i=1:31
  for j=1:41
    
 stress(i,j)=(g(41*(i-1)+j,3))/scale;
end  
 
end



 imagesc(stress')






ylabel('Young modulus (KPa)');
xlabel('Poisson ratio')

hcb=colorbar;
set(hcb,'YTick',[110,115,120,125]);
ylabel(hcb,'principal stress (KPa)')