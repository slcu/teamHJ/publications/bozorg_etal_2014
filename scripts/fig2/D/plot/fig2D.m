M=figure 

load fig2D.data;

D=fig2D;

teta=D(:,1);
s1=D(:,2);
s2=D(:,3);

for i=1:size(s1)
teta(i)=(teta(i)*180)/3.14;
end

for i=1:size(s1)
missesS(i,1)=sqrt(s1(i)*s1(i)+s2(i)*s2(i)-s1(i)*s2(i));
end
shift=400;
for i=1:size(s1)
s2(i)=s2(i)+shift;
end




plot(teta,s1,'r')
hold on
plot(teta,s2,'g')
%plot(teta,missesS)


xlim([0 180])
ylim([520 570])
xlabel('anisotropy direction (deg)');
ylabel('stress (KPa)')

load fig2Dshell.data;
Ds=fig2Dshell;

tetas=Ds(:,1);
s1s=Ds(:,3);
s2s=Ds(:,4);
for i=1:size(s1s)
tetas(i)=(tetas(i)*180)/3.14;
end

for i=1:size(s1s)
missesSs(i,1)=sqrt(s1s(i)*s1s(i)+s2s(i)*s2s(i)-s1s(i)*s2s(i));
end
for i=1:size(s1s)
s2s(i)=s2s(i)+shift;
end



plot(tetas,s1s,'xr')
hold on
plot(tetas,s2s,'xg')
%plot(tetas,missesSs,'xb')
legend('stress1 TRBS','stress2 TRBS','stress1 shell','stress2 shell')