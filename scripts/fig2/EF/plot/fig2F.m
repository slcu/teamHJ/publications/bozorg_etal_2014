clear all

load strainTRBS.dat
load missesStrainShell.dat

A=strainTRBS;
B=missesStrainShell;

strainvector=0:.01:0.1;
areavector=1:0.0145:1.15;

poisson=0.2;

strainmaxTRBS=A(:,1);
strain2ndTRBS=A(:,2);


MissesStrainShell=B(:,2)
sizeTRBS=size(strainmaxTRBS)
sizeshell=size(MissesStrainShell)

for k=1:sizeTRBS
missesStrainTRBS(k)=(1/(1+poisson))*sqrt((strainmaxTRBS(k)^2+strain2ndTRBS(k)^2)-strainmaxTRBS(k)*strain2ndTRBS(k));
end

strainhist(:,1)=histc(missesStrainTRBS,strainvector)/sizeTRBS(1);
strainhist(:,2)=histc(MissesStrainShell,strainvector)/sizeshell(1);
s=bar(strainhist,'group');
title('max strain');
xlim([0 9]);
set(s(1),'facecolor',[0 .8 1])
set(s(2),'facecolor',[0 0 1])
legend('TRBS','Shell')
xlabel('Mises strain');
ylabel('normalized frequency')
MeanTRBS=mean(missesStrainTRBS)
MeanShell=mean(MissesStrainShell)

