clear all

load fig2aP2.data;
load fig2aP4.data;
load fig2aP2shell.data;
load fig2aP4shell.data;

A=fig2aP2;
B=fig2aP4;
C=fig2aP2shell;
D=fig2aP4shell;

def=A(:,1);

for i=1:size(def)
def(i)=100*(def(i)-0.707134)/0.707134;
end

sp2=A(:,3);
sp4=B(:,3);

for i=1:size(def)
sp2(i)=sp2(i)/10;
sp4(i)=sp4(i)/10;
end


plot(def,sp2,'r')
hold on
plot(def,sp4,'g')

defsh=C(:,1);

for i=1:size(defsh)
defsh(i)=100*(defsh(i)-0.707134)/(2*0.707134);
end

sp2sh=C(:,4);
sp4sh=D(:,4);
for i=1:size(defsh)
sp2sh(i)=sp2sh(i)*100;
sp4sh(i)=sp4sh(i)*100;
end

plot(defsh,sp2sh,'+r')
hold on
plot(defsh,sp4sh,'xg')
xlim([0 65])
ylim([0 1100])
xlabel('X deflection (percent)');
ylabel('principal stress (KPa)')


legend('\nu=0.2 TRBS','\nu=0.4 TRBS','\nu=0.2 shell','\nu=0.4 shell')