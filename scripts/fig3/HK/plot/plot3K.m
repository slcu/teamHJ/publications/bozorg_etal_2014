clear all

load fig3K.dat

A=fig3K
angvector=-.8:.05:2;



for j=1:3
    for i=1:length(A(:,1))
      if A(i,j) < -0.7854
         A(i,j)=A(i,j)+3.1416;
      end
    end
end


MT=A(:,1)
stress=A(:,3)
strain=A(:,2)


hist(:,1)=histc(MT,angvector);
hist(:,2)=histc(stress,angvector);
hist(:,3)=histc(strain,angvector);


h=bar3(hist,0.3);

set(h(1),'facecolor','red')
set(h(2),'facecolor','green')
set(h(3),'facecolor','cyan')

for i = 1:numel(h)  
  index = logical(kron(hist(:,i) == 0,ones(6,1)));
  zData = get(h(i),'ZData');
  zData(index,:) = nan;
  set(h(i),'ZData',zData);
end
hFig = figure(1);
set(hFig, 'Position', [400 400  550 550])

xlim([0.6 3.6])
% ylim([2 22])

zlim([0 200])

h=ylabel('angle');
pos=get(h,'pos');
set(h,'pos', [-1 33 0.0])
zlabel('frequency','FontSize',24);
%set(gca,'YTickLabel',{'0 ','pi/2'})
set(gca,'YTick',[16 48])
set(gca,'XTickLabel',{'MT','stress','strain'},'YTickLabel',{'0 ','pi/2'},'FontSize',24)

pbaspect([1.2 3 2])

