clear all
format long
load parascan3A.data;

g=parascan3A;


for i=1:61
  for j=1:51
    
 teta(i,j)=(g(51*(i-1)+j,5));
end  
 
end

colormap(gray)


 imagesc(teta')


ylabel('force anisotropy');
xlabel('elasticity ratio')

hcb=colorbar;
set(hcb,'YTick',[0,1]);
ylabel(hcb,'cos(stress,strain)')