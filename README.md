### Background

This repository host the files needed to reproduce the results in the paper

Bozorg, B., Krupinski, P. and Jönsson, H. (2014) Stress and Strain Provide Positional 
and Directional Cues in Development <i>PLoS Comp Biol</i> 10(1): e1003410.
https://doi.org/10.1371/journal.pcbi.1003410

This repository has been adapted from the Supplemental Archive file provided with the publication,
which also includes all source code at the version where the simulations were originally 
performed (https://doi.org/10.1371/journal.pcbi.1003410.s007).

### Introduction

We have used in house-developed open source software for generating the results of 
the paper. The bi-quadratic spring (plate elements) results have been generated with 
the ['tissue'](https://gitlab.com/slcu/teamhj/tissue), which is a vertex-based software for simulatig 
mechanical and molecular interactions within cell geometries that are allowed to 
grow and proliferate (see e.g. [1,2]). 

The shell Finite Element Model software used is provided as source code for each individual simulation 
in the archive file 'sourceCodes/Cells-lite.tar.gz' within this repository. 

### Software

The tissue software is available via the Sainsbury Laboratory software repository https://gitlab.com/slcu/teamhj/tissue, and instructions for downloading, compiling
and running simulations in general are available in the Documentation in the tissue repository.

To set up for running the simulations specific for this paper, follow the instructions in the Readme files in the 'sourceCodes', and . 
'sourceCodes/tissue/, 'sourceCodes/Cells-lite/' directories.


### Generating the results and figures
 
In the directory 'scripts' you can find all the model/init/solver and plot-files needed 
for generating the result presented in the paper. See the Readme in the 'scripts' 
folder for detailed instructions.

### Contact

Main contact is henrik.jonsson@slcu.cam.ac.uk.

### References

[1] Developmental patterning by mechanical signals in Arabidopsis. O. Hamant, M. 
Heisler, H. Jonsson, P. Krupinski, M. Uyttewaal, P. Bokov, F. Corson, P. Sahlin, A. 
Boudaoud, E. M. Meyerowitz, Y. Couder, and J. Traas. Science 322, 1650-1655 (2008)

[2] A modeling study on how cell division affects properties of epithelial tissues 
under isotropic growth. P. Sahlin, H. Jonsson. PLoS ONE 5(7):e11750 (2010)
