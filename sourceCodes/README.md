## Software used

The Cells-lite.tar.gz includes the shell FEM code.
For the bi-quadratic spring (plate) simulations, the [tissue](https://gitlab.com/slcu/teamhj/tissue) software has been used.

### EXTRACTION AND COMPILATION

TISSUE: 

Download the tissue c++ source code from the Sainsbury Laboratory software repository https://gitlab.com/slcu/teasmhj/tissue/. In the documentation to 'tissue' further instructions on dependencies, compilation, and Documentation is provided in this repository.

Compile the main simulator by writing 'make' in the 'TISSUE/src' directory, where TISSUE is the main folder from the download. This should generate a simulator binary file in the tissue/bin directory.

A tool call auto is also used to run sequences of simulations. This is compiled by writing 'make' in the tissue/tools/auto/ directory, and the 'auto' binary is generated in the folder 'TISSUE/tools/auto/bin'.

The scripts defined to create the results assume that 'tSim' is a globally accessible link to the TISSUE/bin/simulator and that 'auto' can also be called from anywhere. This can be done (in unix/linux/macOSX environments) by linking to correct binaries from your HOME/bin/ directory, and making sure that the HOME/bin/ directory is part of the PATH variable, e.g. by

>cd;cd bin

>ln -s TISSUE/bin/simulator tSim 

>ln -s <PATH TO THIS REPOSITORY>/sourceCodes/tissue/tools/auto/auto . 

NOTE: This repository is adopted from the archive file provided with the publication. There is more than 5 years development of the tissue code, and while we always try to keep it backward compatible ther might be some issues. Either download the tissue code provided in the paper supplemental archive and compile and run that version, or let us know if you encounter some problems.

FEM: For Cells-lite.tar.gz:

Unzip/untar the package, e.g. via 'tar-xzf Cells-lite.tar.gz'.

The software depends on the UMFPACK and CGAL libraries and their internal dependencies.
These utilities have to be installed and their headers have to be available in include path in order
to be able to compile and build the program.  The CMake scripts for building the executables
for generation of the data used in Figures 2 and S1 are provided in the Cells-lite folder.
Building the package will provide the executable files with names corresponding to the
part of the figure for which they generate the data. See the README.txt file in the Cells-lite folder for specific instructions. 
 
